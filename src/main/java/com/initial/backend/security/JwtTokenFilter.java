package com.initial.backend.security;

import com.initial.backend.exception.AppException;
import com.initial.backend.util.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

public class JwtTokenFilter extends GenericFilterBean {
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    private JwtTokenProvider jwtTokenProvider;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
            throws IOException, ServletException, AppException {

        String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
        List<String> roles = jwtTokenProvider.getRoles(token);
        logger.debug("roles {}", roles);
        if(roles==null || roles.size()<=0){
            filterChain.doFilter(req, res);
        }else{
            String role = roles.get(0);
            if(role!=null && role.equalsIgnoreCase(Constant.ADMIN)){
                if (token != null && jwtTokenProvider.validateToken(token)) {
//                    Authentication test = jwtTokenProvider.getAdminAuthentication(token);
                    Authentication auth = null;

                    if (auth != null) {
                        SecurityContextHolder.getContext().setAuthentication(auth);
                    }
                }
            }

            else{
                if (token != null && jwtTokenProvider.validateToken(token)) {
//                    Authentication test = jwtTokenProvider.getUserAuthentication(token);
                    Authentication auth = null;
                    if (auth != null) {
                        SecurityContextHolder.getContext().setAuthentication(auth);
                    }
                }
            }
        }
//        TransferDto role = ((HttpServletRequest) req).getHeader(Constant.ROLE);
        filterChain.doFilter(req, res);
    }
}
