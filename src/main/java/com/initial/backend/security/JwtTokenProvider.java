package com.initial.backend.security;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.initial.backend.entity.User;
import com.initial.backend.service.auth.UserAuthService;
import com.initial.backend.util.Constant;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class JwtTokenProvider {
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Value("${security.jwt.token.secret-key}")
    private String secretKey ;

    Gson gson = new Gson();


//    @Value("${security.jwt.token.expire-length:3600000}")

//    private long validityInMilliseconds = 1000*60*60*24*365;//3600000*24*30*12; // 1h
//    private long validityInMilliseconds = 1000*60*60*24*365; // 1 month

    private long validityInMilliseconds = 31536000000L; // 1 year
    @Autowired
    private UserAuthService userAuthService;

    public String createToken(String stringField, Long userId, List<String> roles) {

        Claims claims = Jwts.claims().setSubject(stringField);
        claims.put("roles", roles);
        claims.put("userId", String.valueOf(userId));


        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds);
        System.out.println(" Token expired "+ validity);

        return Jwts.builder()//
                .setClaims(claims)//
                .setIssuedAt(now)//
                .setExpiration(validity)//
                .signWith(SignatureAlgorithm.HS256, secretKey)//
                .compact();
    }



    public String getLoginId(String authorization) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authorization).getBody().getSubject();
    }

    public List<String> getRoles(String token) {
        try{
            Jwt<JwsHeader, Claims> jwt =  Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);//.getBody();//.get("roles", ArrayList.class);
            Claims claims = jwt.getBody();
            logger.debug(new Gson().toJson(claims));
            Type type = new TypeToken<Map>(){}.getType();
            Map map = gson.fromJson(gson.toJson(claims), type);
            List<String> roles = (List<String>) map.get("roles");
            return roles;
        }catch (Exception e){
            return Arrays.asList(Constant.EMPLOYEE, Constant.EMPLOYER, Constant.ADMIN, Constant.HAMLET, Constant.NEIGHBOURHOOD);
        }
    }

    public Long getUserId(String token) {
        try{
            Jwt<JwsHeader, Claims> jwt =  Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);//.getBody();//.get("roles", ArrayList.class);
            Claims claims = jwt.getBody();
            logger.debug(new Gson().toJson(claims));
            Type type = new TypeToken<Map>(){}.getType();
            Map map = gson.fromJson(gson.toJson(claims), type);
            if(map.get("userId")!=null){
                String userIdString = (String) map.get("userId");
                return Long.valueOf(userIdString);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null ;
    }


    public Long getEmployerId(String token) {
        try{
            Jwt<JwsHeader, Claims> jwt =  Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            Claims claims = jwt.getBody();
            logger.debug(new Gson().toJson(claims));
            Type type = new TypeToken<Map>(){}.getType();
            Map map = gson.fromJson(gson.toJson(claims), type);
            if(map.get("employerId")!=null){
                String userIdString = (String) map.get("employerId");
                return Long.valueOf(userIdString);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null ;
    }


    public Long getEmployeeId(String token) {
        try{
            Jwt<JwsHeader, Claims> jwt =  Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            Claims claims = jwt.getBody();
            logger.debug(new Gson().toJson(claims));
            Type type = new TypeToken<Map>(){}.getType();
            Map map = gson.fromJson(gson.toJson(claims), type);
            if(map.get("employeeId")!=null){
                String userIdString = (String) map.get("employeeId");
                return Long.valueOf(userIdString);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null ;
    }

    public Authentication getUserAuthentication(String token){
        UserDetails userDetails = this.userAuthService.loadUserByEmail(getLoginId(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public User getUser(String token){
        Authentication authentication = getUserAuthentication(token);
        User user = (User) authentication.getPrincipal();
        return user;
    }

    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader(HttpHeaders.AUTHORIZATION);
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return bearerToken;
    }

    public boolean validateToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);

            if (claims.getBody().getExpiration().before(new Date())) {
                return false;
            }

            return true;
        } catch (JwtException | IllegalArgumentException e) {
            return false ;
        }
    }
}
