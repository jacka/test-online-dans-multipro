package com.initial.backend.model.misc;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Collections;

@Data
public class GeneralResponse<T> {

    Integer code;
    T data;
    String message;

    public static <T>GeneralResponse<T> response(T data){
        if(data!=null){
            if(data.getClass()==null || data.equals(Collections.emptyList())){
                return GeneralResponse.dialog(400, "data empty");
            }
        }
        GeneralResponse<T> resp = new GeneralResponse<>();
        resp.setCode(200);
        resp.setData(data);
        return resp;
    }

    public static <T> GeneralResponse <T> response (int code, String message, T data){
        GeneralResponse<T> response = new GeneralResponse<>();
        response.setCode(code);
        response.setMessage(message);
        response.setData(data);
        return response;
    }

    public static <T> GeneralResponse <T> response (HttpStatus code, T data, String message){
        GeneralResponse<T> response = new GeneralResponse<>();
        response.setCode(code.value());
        response.setMessage(message);
        response.setData(data);
        return response;
    }

    public static <T>GeneralResponse<T> dialog(Integer code, String message){
        GeneralResponse<T> resp = new GeneralResponse<>();
        resp.setCode(code);
        resp.setMessage(message);
        return resp;
    }

}
