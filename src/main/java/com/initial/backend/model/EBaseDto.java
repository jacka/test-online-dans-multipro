package com.initial.backend.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initial.backend.util.JsonDateDeserializer;
import com.initial.backend.util.JsonDateSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.initial.backend.config.DateAppConfig;
import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@Data
public class EBaseDto implements Serializable {

    Long id ;

    @ApiModelProperty(example = DateAppConfig.API_DATE_EXAMPLE)
//    @JsonFormat(pattern = DateAppConfig.API_DATE_FORMAT)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date created ;
    Long createdBy ;
    @ApiModelProperty(example = DateAppConfig.API_DATE_EXAMPLE)
//    @JsonFormat(pattern = DateAppConfig.API_DATE_FORMAT)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date updated ;
    Long updatedBy ;
    Boolean active ;

    public EBaseDto(){
//        if(createdBy==null){
//            createdBy = Constant.SYSTEM;
//        }
//        if(updatedBy==null){
//            updatedBy = Constant.SYSTEM;
//        }
//        if(created==null){
//            created = new Date();
//        }
//        updated = new Date();
//        if(active==null){
//            active = Boolean.TRUE;
//        }

        if(id==null || id.equals(0) || id == 0){
//            Random random = new Random(System.nanoTime());
//            id = random.nextLong();
            id = null ;
        }
    }

    public Long getId() {
        if(id==null || id.equals(0) || id==0){
//            Random random = new Random(System.nanoTime());
//            id = random.nextLong();
            id = null ;

        }
        return id;
    }

    public void setId(Long id) {
        if(id==null || id.equals(0) || id==0){
//            Random random = new Random(System.nanoTime());
//            id = random.nextLong();
            id = null ;

        }
        this.id = id;
    }

//    @Override
//    public int hashCode() {
//        if(id==null){
//            return (int)System.nanoTime();
//        }
//        return id.intValue();
//    }

//    @Override
//    public boolean equals(Object obj) {
//        // it checks if the argument is of the
//        // type Geek by comparing the classes
//        // of the passed argument and this object.
//        // if(!(obj instanceof Geek)) return false; ---> avoid.
//        try {
//            if(obj == null)
//                return false;
//
//            if(this == obj)
//                return true;
//
//            // type casting of the argument.
//            EBaseDto eBase = (EBaseDto) obj;
//
//            // comparing the state of argument with
//            // the state of 'this' Object.
//            return (eBase.id!=null) && (this.id!=null) && (eBase.id == this.id) ;
//        }catch (Exception e){
//            return false ;
//        }
//    }
}
