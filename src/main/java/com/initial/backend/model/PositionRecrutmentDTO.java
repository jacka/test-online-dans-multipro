package com.initial.backend.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

//@JsonInclude(JsonInclude.Include.NON_NULL)
//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Data
public class PositionRecrutmentDTO implements Serializable {
    String id;
    String type;
    String url;

    @JsonProperty("created_at")
    String created_at;
    String company;
    @JsonProperty("company_url")
    String company_url;
    String location;
    String title;
    String description;
    @JsonProperty("how_to_apply")
    String how_to_apply;
    @JsonProperty("company_logo")
    String company_logo;

}
