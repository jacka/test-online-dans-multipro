package com.initial.backend.repository.misc;

import com.initial.backend.entity.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findByActive(Boolean active, Sort sort);
    Optional<User> findByEmail(String email);
    User findAllByEmailAndActiveIsTrue(String email);
}
