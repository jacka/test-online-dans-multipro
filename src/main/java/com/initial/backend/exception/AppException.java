package com.initial.backend.exception;

import lombok.Getter;
import lombok.Setter;

public class AppException extends RuntimeException {
    @Getter
    @Setter
    private int code ;

    public AppException(int code, String message){
        super(message);
        this.code = code;
    }

}
