package com.initial.backend.controller;

import com.google.gson.Gson;
import com.initial.backend.exception.AppException;
import com.initial.backend.security.JwtTokenProvider;
import com.initial.backend.util.RestUtils;
import com.initial.backend.util.ValidationBuilder;
import com.initial.backend.model.WSResponse;
import com.initial.backend.util.Constant;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.commons.lang.ArrayUtils;
import org.hibernate.QueryException;
import org.hibernate.TransientPropertyValueException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.persistence.MappedSuperclass;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@MappedSuperclass
public class BasicController<E> {
    private static final Logger logger = LoggerFactory.getLogger(BasicController.class.getName());

    @Autowired MessageSource messageSource ;
    @Autowired protected Gson gson ;
    @Autowired protected ModelMapper modelMapper ;
    @Autowired LocalValidatorFactoryBean validator ;
    @Autowired protected HttpServletRequest request ;
    @Autowired protected JwtTokenProvider jwtTokenProvider ;
    @Autowired protected RestUtils restUtils ;

    @Value("${page.row}")
    protected int pageRow ;

    @PostConstruct
    public void postConstruct(){
    }

    public Type type(){
        return null ;
    }

    public Type typeList(){
        return null ;
    }

    public WSResponse findAll(Boolean ascending, String sortir){
        return WSResponse.instance(0, "");
    }

    public WSResponse findById(Long id){
        return WSResponse.instance(0, "");
    }

    public WSResponse save(E ob){
        return WSResponse.instance(0, "");
    }

    public WSResponse save(E ob, MultipartFile multipartFile) throws Exception{
        return WSResponse.instance(0, "");
    }
    public WSResponse save(String jsonObjectInString, MultipartFile multipartFile) throws Exception{
        return WSResponse.instance(0, "");
    }

    public WSResponse delete(Long id){
        return WSResponse.instance(0, "");
    }

    protected String message(String message, Object... args) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(message, args==null?new Object[]{}:args, locale);
    }

    public <T> void validate(T object){
        Set<ConstraintViolation<T>> constrains = validator.validate(object);
        List<String> strings = new ArrayList<>();
        for (ConstraintViolation<T> constrain : constrains) {
            String objectError = constrain.getMessage();
            strings.add(objectError);
        }
        if(strings.size()>0){
            throw new RuntimeException(ArrayUtils.toString(strings));
        }
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, IllegalArgumentException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        if(e.getMessage().contains(QueryException.class.getName())){
            return WSResponse.instance(Constant.FAILED_CODE, QueryException.class.getName());
        }
        return WSResponse.instance(Constant.FAILED_CODE, e.getLocalizedMessage());
    }


    @ExceptionHandler(value = RuntimeException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, RuntimeException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        if(e.getMessage().contains(QueryException.class.getName())){
            return WSResponse.instance(Constant.FAILED_CODE, QueryException.class.getName());
        }
        return WSResponse.instance(Constant.FAILED_CODE, e.getMessage());
    }

    @ExceptionHandler(value = ExpiredJwtException.class)
    public WSResponse expiredJwtExceptionHandler(HttpServletRequest req, ExpiredJwtException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return WSResponse.instance(Constant.JWT_SESSION_EXPIRED_CODE, "This session has been expired, please resign in");
    }


    @ExceptionHandler(value = Exception.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return WSResponse.instanceError(Constant.FAILED_CODE, e.getMessage(), e);
    }

    @ExceptionHandler(value = AppException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, AppException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return WSResponse.instance(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, MethodArgumentNotValidException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return ValidationBuilder.validate(e.getBindingResult());
    }


    @ExceptionHandler(value = TransientPropertyValueException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, TransientPropertyValueException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        String detailMessage = "If you want to save an object which is has been stored in a database, please provide an id of this object, or you can save this object before calling this api";
        return WSResponse.instanceError(Constant.FAILED_CODE, e.getMessage(), detailMessage);
    }

    @ExceptionHandler(value = InvalidDataAccessApiUsageException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, InvalidDataAccessApiUsageException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        String detailMessage = "If you want to save an object which is has been stored in a database, please provide an id of this object, or you can save this object before calling this api";
        return WSResponse.instanceError(Constant.FAILED_CODE, detailMessage, e);
    }


    @ResponseBody
    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    public WSResponse handleHttpMediaTypeNotAcceptableException() {
        return WSResponse.instance(Constant.FAILED_CODE, Constant.FAILED);
    }

    public ResponseEntity excelEntity(String filename, byte[] fileInBytes){
        ByteArrayResource byteArrayResource = new ByteArrayResource(fileInBytes);
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=\""+filename+"\"")
                .header("filename", filename)
                .body(byteArrayResource);
    }

}
