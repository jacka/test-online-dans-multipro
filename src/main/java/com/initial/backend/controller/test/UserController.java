package com.initial.backend.controller.test;

import com.initial.backend.annotation.AdminRest;
import com.initial.backend.annotation.FrontendRest;
import com.initial.backend.annotation.UniversalRest;
import com.initial.backend.controller.BasicController;
import com.initial.backend.entity.User;
import com.initial.backend.entity.UserDto;
import com.initial.backend.exception.AppException;
import com.initial.backend.model.WSResponse;
import com.initial.backend.service.test.UserService;
import com.initial.backend.util.Constant;
import com.initial.backend.util.EncryptionUtils;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;

@RestController
@RequestMapping(path = "${api}")
public class UserController extends BasicController {

    public static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @Transactional
    @PostMapping(path = "/customer/save")
    @AdminRest
    @ApiOperation(value = "api untuk membuat/edit user")
    public WSResponse save(@RequestBody @Valid UserDto userData)throws Exception{
        User user = UserDto.to(userData);
        if(userData.getId()==null || userData.getId().equals(0) || userData.getId()==0){
            Boolean result =  userService.checkFindByEmailIsRegister(userData.getEmail());
            if(result){
                throw new AppException(Constant.FAILED_CODE, "akun atas email"+userData.getEmail()+" sudah digunakan, silahkan ganti email!");
            }

            restUtils.validationPassword(user.getPassword());

            String password = user.getPassword();
            String encrypPassword = EncryptionUtils.encrypt(Constant.AVENGER_KEY, password);
            user.setPassword(encrypPassword);
        }

        restUtils.validationEmail(user.getEmail());
        user = userService.save(user);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
    }

    @GetMapping(path = "/customers")
    @FrontendRest@AdminRest@UniversalRest
    @ApiOperation(value = "api untuk mendapatkan list user")
    public WSResponse findAll(
            @RequestParam(required = false) Boolean active,
            @RequestParam(defaultValue = "id") String sortir,
            @RequestParam(defaultValue = "true") Boolean ascending
    ){
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, userService.findAll(active, sortir, ascending));
    }

    @GetMapping(path = "/customer")
    @FrontendRest@AdminRest@UniversalRest
    @ApiOperation(value = "api untuk mendapatkan object user")
    public WSResponse findById(@RequestParam Long id
    ){
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, userService.findById(id));
    }

    @GetMapping(path = "/customer/info")
    @FrontendRest@AdminRest@UniversalRest
    @ApiOperation(value = "api untuk mendapatkan user berdasarkan token")
    public WSResponse info(
            @RequestHeader(Constant.AUTHORIZATION) String authorization
    ){

        User user = jwtTokenProvider.getUser(authorization);

        if(user==null){
            throw new AppException(Constant.FAILED_CODE, "User tidak ditemukan");
        }

        if(!user.getActive()){
            throw new AppException(Constant.FAILED_CODE, "User telah di nonaktifkan");
        }

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
    }

    @Transactional
    @PostMapping(path = "/customer/signin")
    @AdminRest@FrontendRest
    @ApiOperation(value = "api untuk login user")
    public WSResponse signin(@RequestParam String email, @RequestParam String password)throws Exception{
        User user = userService.signin(email);

        String enncryptedPassword = EncryptionUtils.encrypt(Constant.AVENGER_KEY, password);
        if(!user.getPassword().equalsIgnoreCase(enncryptedPassword)){
            throw new AppException(Constant.FAILED_CODE, "password yang and masukan salah!");
        }

        if(!user.getActive()){
            throw new AppException(Constant.FAILED_CODE, "User telah di nonaktifkan");
        }

        String token = jwtTokenProvider.createToken(email, user.getId(), Arrays.asList("user"));
        user.setAuthorization(token);


        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
    }


}
