package com.initial.backend.controller.test;

import com.initial.backend.annotation.AdminRest;
import com.initial.backend.annotation.FrontendRest;
import com.initial.backend.annotation.UniversalRest;
import com.initial.backend.controller.BasicController;
import com.initial.backend.exception.AppException;
import com.initial.backend.model.PositionRecrutmentDTO;
import com.initial.backend.model.WSResponse;
import com.initial.backend.model.misc.DataPage;
import com.initial.backend.service.test.PositionRecrutmentService;
import com.initial.backend.util.Constant;
import com.initial.backend.util.Utils;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(path = "${api}")
public class PositionRecrutmentController extends BasicController {

    @Autowired
    PositionRecrutmentService positionRecrutmentService;

    @GetMapping(path = "/positionRecrutments")
    @FrontendRest
    @AdminRest
    @UniversalRest
    @ApiOperation(value = "api untuk mendapatkan list posisi rekrutmen")
    public WSResponse findAll(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam(defaultValue = "0") Integer page,
                                  @RequestParam(defaultValue = "id") String sortir,
                                  @RequestParam(defaultValue = "false") Boolean ascending,
                                  @RequestParam(defaultValue = "false") Boolean list
                              )throws Exception{
        DataPage dataPage = positionRecrutmentService.findAll(list, page, sortir, ascending);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dataPage.getDatas())
                .setPageElement(Constant.ROW_LIMIT)
                .setTotalElement(dataPage.getTotalElement())
                .setTotalPage(dataPage.getTotalPage());

    }

    @GetMapping(path = "/positionRecrutment")
    @FrontendRest@AdminRest@UniversalRest
    @ApiOperation(value = "api untuk mendapatkan posisi rekrutmen")
    public WSResponse findById(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam String id
    ){
        PositionRecrutmentDTO positionRecrutmentDTO = positionRecrutmentService.getOne(id);
        if(positionRecrutmentDTO==null){
            throw new AppException(Constant.FAILED_CODE, "Data posisi pelamar tidak ditemakan!");
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, positionRecrutmentService.getOne(id));
    }
}
