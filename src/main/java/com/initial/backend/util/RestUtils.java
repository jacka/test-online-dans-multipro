package com.initial.backend.util;


import com.initial.backend.exception.AppException;
import com.initial.backend.security.JwtTokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class RestUtils {

    private static final Logger logger= LoggerFactory.getLogger(RestUtils.class);

    @Autowired
    JwtTokenProvider jwtTokenProvider ;

    public <UD extends UserDetails> UD user(String authorization){
        if(authorization==null){
            throw new AppException(Constant.FAILED_CODE, "Authorization identifier is not found");
        }
        List<String> roles = jwtTokenProvider.getRoles(authorization);
        String role = roles.get(0);
        if(role.equalsIgnoreCase(Constant.ADMIN)){

        }

        return null ;
    }

    public Boolean isRole(String authorization, String role){
        List<String> roles = jwtTokenProvider.getRoles(authorization);
        if(roles!=null && roles.size()>=0){
            String tokenRole = roles.get(0);
            if(tokenRole.equalsIgnoreCase(Constant.ADMIN) && tokenRole.equalsIgnoreCase(role)){
                return true ;
            }
            else{
//                if(!tokenRole.equalsIgnoreCase(role) && !tokenRole.equalsIgnoreCase(Constant.EMPLOYEE_AND_EMPLOYER)){
//                    return false ;
//                }
                return true ;
            }
        }else{
            return false ;
        }
    }


    public void validateRoleException(String authorization, String role){
        List<String> roles = jwtTokenProvider.getRoles(authorization);
        logger.debug("ROLES {} ", roles);
        if(roles!=null && roles.size()>=0){
            String tokenRole = roles.get(0);
            if(tokenRole.equalsIgnoreCase(Constant.ADMIN) && tokenRole.equalsIgnoreCase(role)){

            }
            else{
//                if(!tokenRole.equalsIgnoreCase(role) && !tokenRole.equalsIgnoreCase(Constant.EMPLOYEE_AND_EMPLOYER)){
//                    throw new AppException(Constant.FAILED_CODE, tokenRole+" shouldn't access this api");
//                }
            }
        }else{
            throw new AppException(Constant.FAILED_CODE, " role is not found");
        }
    }

    public void validationEmail(String email){
        Pattern emailPattern = Pattern.compile(Constant.EMAIL_PATTERN);
        Matcher isEmail = emailPattern.matcher(email);
        if(!isEmail.find()){
            throw new AppException(Constant.FAILED_CODE, email+" format email tidak sesuai!");
        }
    }

    public void validationPassword(String password){
        if(StringUtils.isEmpty(password)){
            throw new AppException(Constant.FAILED_CODE, "password tidak boleh kosong!");
        }

        if(password.length()<8){
            throw new AppException(Constant.FAILED_CODE, "password tidak boleh kurang dari 8 digit!");
        }
    }
}


