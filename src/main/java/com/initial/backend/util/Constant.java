package com.initial.backend.util;

public class Constant {
    public static final String AUTHORIZATION = "Authorization";
    // api BCA
    public static final String BCA_KEY = "X-BCA-Key";
    public static final String BCA_SIGNATURE = "X-BCA-Signature";
    public static final String BCA_TIMESTAMP = "X-BCA-Timestamp";
    public static final String URL_SANDBOX_BCA = "https://sandbox.bca.co.id:443";
    public static final String URL_DEVELOPMENT_BCA = "https://devapi.klikbca.com:443";
    public static final String URL_PRODUCTION_BCA = "https://api.klikbca.com:443";
    public static final String URL_BCA = URL_PRODUCTION_BCA;
    public static final String PAYMENT_FLAG_STATUS_SUCCESS = "00";
    public static final String PAYMENT_FLAG_STATUS_FAILED = "01";

    public static final String ALL = "ALL";
    public static final String ACTIVE = "ACTIVE";
    public static final String EXPIRED = "EXPIRED";
    public static final String DDMMYYYY_HH_MM_SS = "ddMMyyyy_hh_mm_ss";


    public static final String AVAILABLE_DISCOUNT_DATE  = ALL+","+ACTIVE+","+EXPIRED;

    public static final Long MINIMUM_SALARY_EMPLOYEE = 10000L;

    public static final Long SYSTEM = 1l;
    public static final Long SYSTEM_REGISTER = 1l;
    public static final String MASTER_PASSWORD = "Ecommerce%Master" ;

    public static final String DD_MM_YYYY_HH_MM = "dd-MM-yyy hh:mm";


    public static final int SUCCESS_CODE = 200 ;
    public static final int FAILED_CODE = 300 ;
    public static final int VERIFICATION_FAILED_CODE = 302 ;
    public static final int ALREADY_VERIFIED_CODE = 303 ;
    public static final int JWT_SESSION_EXPIRED_CODE = 301 ;
    public static final int IMAGE_NOT_FOUND = 301 ;
    public static final int FILE_NOT_FOUND = 301 ;
    public static final int SMS_VERIFICATION_REACH_LIMIT = 301 ;
    public static final String SUCCESS = "SUCCESS" ;
    public static final String DELETED_SUCCESSFULLY = "DELETED_SUCCESSFULLY" ;
    public static final String FAILED = "FAILED" ;
    public static final String ALREADY_EXISTS = "ALREADY_EXISTS" ;

    //    public static final TransferDto EMAIL_PATTERN = "^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$";
    public static final String EMAIL_PATTERN = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    public static final String ID_PATTERN = "[0-9]+";
    public static final String ALPHABET_PATTERN = "^[a-zA-Z]*$";
    public static final String ALPHABET_SPACE_PATTERN = "^(?![ ]+$)[a-zA-Z ]*$";
    public static final String NUMERIC_PATTERN = "^[0-9]*$";
    public static final String ALPHABET_LOWER_PATTERN = "^[a-z]*$";

    public static final Integer ROW_LIMIT = 10;

    public static final String LIMIT_20 = "20";
    public static final String LIMIT_30 = "30";
    public static final String LIMIT_40 = "10";


    public static final String LIMIT_PAGE  = LIMIT_20+","+LIMIT_30+","+LIMIT_40;


//    public static final TransferDto REST_IMAGE_PHOTO_PROFILE = "image/photoProfile";
//    public static final TransferDto REST_IMAGE_IDCARD = "image/idcard";
//    public static final TransferDto REST_IMAGE_TRANSFER_BILL = "image/transferBill";



    public static final String MASTER_JOB_CATEGORY_JSON_FILE = "json/job-category.json";
    public static final String MASTER_WORKING_DAY_JSON_FILE = "json/working-day.json";
    public static final String MASTER_LONG_TIME_EXPERIENCE_JSON_FILE = "json/long-time-experience.json";
    public static final String MASTER_EMPLOYEE_STATUS_JSON_FILE = "json/employee-status.json";
    public static final String MASTER_EMPLOYER_STATUS_JSON_FILE = "json/employer-status.json";
    public static final String MASTER_CORPORATE_TYPE_JSON_FILE = "json/corporate-type.json";
    public static final String MASTER_PERSONALITY_JSON_FILE = "json/personality.json";
    public static final String MASTER_PSYCHOTEST_JSON_FILE = "json/psychotest.json";
    public static final String MASTER_PRODUCT_JSON_FILE = "json/product.json";
    public static final String MASTER_SELF_PROMOTION_JSON_FILE = "json/self-promotion.json";
    public static final String MASTER_PURCHASE_STATUS_JSON_FILE = "json/purchase-status.json";
    public static final String MASTER_JOB_APPLICATION_STATUS_JSON_FILE = "json/job-application-status.json";
    public static final String MASTER_SPINNING_ITEM_JSON_FILE = "json/spinning-item.json";
    public static final String MASTER_SUPER_ADMIN_JSON_FILE = "json/super-admin.json";
    public static final String MASTER_COMMENT_JSON_FILE = "json/comment.json";
    public static final String MASTER_REPORT_STATUS_JSON_FILE = "json/report-status.json";


    public static final String REST_CASHIER_PHOTO = "/image/cashier/photo";
    public static final String REST_MITRA_PHOTO = "/image/mitra/photo";
    public static final String REST_SUPER_CATEGORY_ICON = "/image/super-category/icon";
    public static final String REST_CATEGORY_ICON = "/image/category/icon";
    public static final String REST_PRODUCT_PHOTO = "/image/product/photo";

    public static final String REST_BANK_IMAGE = "/image/bank";
    public static final String REST_USER_PHOTO = "/image/user/photo";
    public static final String REST_COMPANY_LOGO = "/image/company/logo";
    public static final String REST_EMPLOYER_SIUP_OR_NPWP = "/image/company/siupOrNpwp";
    public static final String REST_EMPLOYER_ID_CARD = "/image/employer/idcard";
    public static final String REST_EMPLOYEE_ID_CARD = "/image/employee/idcard";
    public static final String REST_EMPLOYEE_SELFIE_ID_CARD = "/image/employee/selfieIdCard";
    public static final String REST_EMPLOYEE_FAMILY_CARD = "/image/employee/familyCard";
    public static final String REST_EMPLOYEE_SKCK = "/image/employee/skck";
    public static final String REST_EMPLOYEE_DIPLOMA = "/image/employee/diploma";
    public static final String REST_EMPLOYEE_CERTIFICATE = "/image/employee/certificate";
    public static final String REST_EMPLOYEE_SIM = "/image/employee/sim";
    public static final String REST_EMPLOYEE_BANNER_IMAGE = "/image/employeeBanner";
    public static final String REST_EMPLOYER_ARTICLE_IMAGE = "/image/employerArticle";
    public static final String REST_EMPLOYER_UPDATE_INFO_IMAGE = "/image/employerUpdateInfo";
    public static final String REST_JOB_CATEGORY_ICON = "/image/job-category";
    public static final String REST_EMPLOYER_PURCHASE_RECEIPT_IMAGE = "/image/employer/purchase/receipt";
    public static final String REST_EMPLOYEE_PURCHASE_RECEIPT_IMAGE = "/image/employee/purchase/receipt";
    public static final String REST_EMPLOYEE_REPORT_IMAGE = "/image/employee/report/image";
    public static final String REST_EMPLOYEE_REPORT_DOCUMENT = "/image/employee/report/document";
    public static final String REST_EMPLOYER_REPORT_IMAGE = "/image/employer/report/image";
    public static final String REST_EMPLOYER_REPORT_DOCUMENT = "/image/employer/report/document";
    public static final String REST_CONFIGURATION_WORKING_AGREEMENT_DOCUMENT = "/image/configuration/workingAgreement";
    public static final String REST_TEMPORARY_IMAGE = "/image/temporary";

    public static final String REST_EMPLOYER_INVOICE_RECEIPT_IMAGE = "/image/employer/invoice/receipt";

    //file
    public static final String REST_EMPLOYEE_WORKING_AGREEMENT_FILE = "/file/employee/workingAgreement/sjc";
    public static final String REST_EMPLOYEE_SKCK_FILE = "/file/employee/skck";
    public static final String REST_EMPLOYEE_FAMILY_CARD_FILE = "/file/employee/familyCard";
    public static final String REST_EMPLOYEE_DIPLOMA_FILE = "/file/employee/diploma";
    public static final String REST_EMPLOYEE_CERTIFICATE_FILE = "/file/employee/certificate";
    //end file

    public static final Integer MAX_AUTOMATICALLY_JOB_APPLY = 20;

    public static final Integer PRODUCTION = 1;
    public static final Integer DEVELOPMENT = 0;


    public static final Integer BUILD_MODE = DEVELOPMENT;

    public static final String DEVELOPMENT_MODE_NETFISI = "http://139.162.21.28:8071";

    public static final String PRODUCTION_MODE_NETFISI = "http://139.162.21.28:8077";

    public static final String DEVELOPMENT_MODE_PINOPIJOB = "8050";
    public static final String PRODUCTION_MODE_PINOPIJOB = "8051";

    public static final String NO_TELEPON_COMPANY_PINOPI = "081325873239";

//    public static final TransferDto PASSWORD_ENCRYPTION_KEY = "InsyaAllahBermanfaatUntukOrangLain";

    public static final String STATUS_ACCOUNT_HAMLET = "RW";
    public static final String STATUS_ACCOUNT_NEIGHBOURHOOD = "RT";
    public static final String STATUS_ACCOUNT_COMPANY = "COMPANY";
    public static final String STATUS_ACCOUNT_EMPLOYEE = "EMPLOYEE";

    public static final String CLIENT_HEADER_KEY = "InsyaAllahBermanfaatUntukOrangLain";

    public static final String TRIGERRED_CALL = "call";
    public static final String TRIGERRED_MESSAGES = "message";

    public static final String ROLE = "role";
    public static final String UNDEFINED = "undefined";
    public static final String EMPLOYEE = "employee";
    public static final String EMPLOYER = "employer";
    public static final String EMPLOYEE_AND_EMPLOYER = "employee_and_employer";
    public static final String ADMIN = "admin";
    public static final String HAMLET = "hamlet";
    public static final String NEIGHBOURHOOD = "neighbourhood";
    public static final String SWAGGER = "swagger";

    public static final String PERSONAL = "personal";
    public static final String COMPANY = "company";


    public static final String RESET_PASSWORD_TML = "reset_password";
    public static final String RESET_PASSWORD_SUCCESS_TML = "reset_password_success";

    public static final float VERY_LOW_QUALITY = 0.2f;
    public static final float MEDIUM_QUALITY = 0.35f;
    public static final float HIGH_QUALITY = 0.7f;

    public static final String CACHE_CONFIGURATION = "configuration";

    public static final String CACHE_CUSTOMER_ID = "customerId";
    public static final String CACHE_USER_PRODUCTS = "userProducts";
    public static final String CACHE_USER_DASHBOARD = "userDashboard";
    public static final String CACHE_BANNERS = "banners";
    public static final String CACHE_HEADINGS = "headings";
    public static final String CACHE_SELEBGRAMS = "selebgrams";
    public static final String CACHE_ONLINE_SHOPS = "onlineShops";


    public static final String CURRENCY_SYMBOL = "Rp ";

    public static final String MALE = "male";
    public static final String FEMALE = "female";
    public static final String ANY = "any";

    public static final String GENDERS = MALE+","+FEMALE;

    public static final String ENDPOINT_FILE = "/file";

    public static final String WORKING_AGREEMENT_EMPLOYEE = "working_agreement";
    public static final String PHOTO = "photo";
    public static final String ID_CARD = "id_card";
    public static final String SELFIE_ID_CARD = "selfie_id_card";
    public static final String PHOTO_PROFILE = "photo_profile";
    public static final String FAMILY_CARD = "family_card";
    public static final String SKCK = "skck";
    public static final String DIPLOMA = "diploma";
    public static final String CERTIFICATE_1 = "certificate_1";
    public static final String CERTIFICATE_2 = "certificate_2";
    public static final String CERTIFICATE_3 = "certificate_3";
    public static final String SIM = "sim";
    public static final String COMPANY_LOGO = "company_logo";
    public static final String SIUP_OR_NPWP = "siup_or_npwp";

    public static final String DIPLOMA_OR_CERTIFICATE = "diploma_or_certificate";

    public static final String SENDER_SATU = "6281290846396";
    public static final String SENDER_DUA = "";
    public static final String BROASCAST_SERVICE = SENDER_SATU+", "+SENDER_DUA;


    public static final String EMPLOYER_DOCUMENT_VERIFICATION_TYPE = ID_CARD+", "+SIUP_OR_NPWP+", "+PHOTO_PROFILE;
    public static final String EMPLOYEE_DOCUMENT_VERIFICATION_TYPE = ID_CARD+", "+FAMILY_CARD+", "+SKCK+", "+DIPLOMA_OR_CERTIFICATE+", "+PHOTO_PROFILE;
    public static final String IMAGE_UPLOAD_TYPE = PHOTO+", "+ID_CARD+", "+SELFIE_ID_CARD+", "+FAMILY_CARD+", "+SKCK+", "+DIPLOMA+", "+CERTIFICATE_1+", "+CERTIFICATE_2+", "+CERTIFICATE_3+", "+SIM+", "+COMPANY_LOGO+", "+SIUP_OR_NPWP+", "+WORKING_AGREEMENT_EMPLOYEE;


    public static final String STATUS_REJECTED = "rejected";
    public static final String STATUS_UNUPLOAD = "unupload";
    public static final String STATUS_UNVERIFIED = "unverified";
    public static final String STATUS_VERIFIED = "verified";

    public static final String STATUS_DOCUMENT_EMPLOYEE = STATUS_VERIFIED+","+STATUS_UNUPLOAD+","+STATUS_UNVERIFIED+","+STATUS_REJECTED;
    public static final String STATUS_DOCUMENT = STATUS_REJECTED+","+STATUS_UNVERIFIED;


    public static final String COMMON_TOPIC = "common";
    public static final String PAYMENT_STATUS_TOPIC = "payment_status";
    public static final String APPLICATION_STATUS_TOPIC = "application_status";
    public static final String JOB_VACANCY_TOPIC = "job_vacancy";
    public static final String ACCOUNT_STATUS_TOPIC = "account_status";
    public static final String NEWS_TOPIC = "news";
    public static final String ARTICLE_TOPIC = "article";
    public static final String CURRENT_PACKAGE_TOPIC = "current_package";

    public static final String FIREBASE_TOPIC_SWAGGER_NOTE = "<b>( Available Topic )</b>\n" +
            "-- common ==> desc = Only for test \n" +
            "-- news ==> desc = Both employee & employer must subscribe it to get notif everytime a news  published \n" +
            "-- article ==> desc = Both employee & employer must subscribe it to get notif everytime an article published \n" ;


    public static final String AVENGER_KEY = "TindakanPenyusupanAdalahPelanggaran,DanKamiAkanMelaporkanTPelanggaranSecepatnyaKepadaPihakYangBerwajib!!)(*&^%$#@!";


    public static final String FIRST_REGISTERED_ADDRESS = "First registered address ";

    public static final String PURCHASE_DEPOSITE_PACKAGE = "Pembelian Paket Deposit Pinopi";

    public static final String INCOME_FROM_SALARY = " Pendapatan Gaji Pekerja ";

//    public static final TransferDto FIREBASE_TOPIC_SWAGGER_NOTE = "<br>( Available Topic )</b>\n" +
//            "1 = common, desc = Only for test \n" +
//            "2 = payment_status, desc = Both employee & employer must subscribe it to get notif everytime their payment status changed \n" +
//            "3 = application_status, desc = employee could get notif everytime his job application status changed \n" +
//            "4 = job_vacancy, desc = employer could get notif everytime his job vacancy status changed \n" +
//            "5 = account_status, desc = Both employee & employer must subscribe it to get notif everytime their account status changed \n" +
//            "2 = news, desc = Both employee & employer must subscribe it to get notif everytime a news  published \n" +
//            "3 = article, desc = Both employee & employer must subscribe it to get notif everytime an article published \n" ;
//            "7 = current_package, desc = Both employee & employer must subscribe it to get notif everytime their package / product status changed \n" ;

}
