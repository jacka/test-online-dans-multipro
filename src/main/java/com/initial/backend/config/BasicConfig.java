package com.initial.backend.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.initial.backend.annotation.AdminRest;
import com.initial.backend.annotation.FrontendRest;
import com.initial.backend.annotation.UniversalRest;
import com.initial.backend.component.MyRequestInterceptor;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Locale;

@EnableJpaRepositories
@EnableSwagger2
@ComponentScan
@Configuration
@PropertySource("classpath:application.properties")
public class BasicConfig {

    private static final Logger logger4j = LoggerFactory.getLogger(BasicConfig.class);

    private static final String SWAGGER_API_VERSION = "1.0";
    private static final String LICENSE_TEXT = "License";
    private static final String title = "Products REST API";
    private static final String description = "RESTful API for Products";


    @Bean
    public Docket frontendApi() {
        return new Docket(DocumentationType.SWAGGER_12)
                .groupName("1 - Frontend Documentation")
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(FrontendRest.class))
                .paths(PathSelectors.any())
                .build().apiInfo(
                        new ApiInfoBuilder()
                                .title("Test Online Dans Multi Pro Frontend Documentation")
                                .description("API Documentation with json structure / formatting," +
                                        "Rest Method, e.t.c for KSI Application")
                                .license("Apache 2.0")
                                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                                .termsOfServiceUrl("")
                                .version("1.0.0")
                                .contact(new Contact("", "", "support@test.com"))
                                .build()
                );
    }

    @Bean
    public Docket adminApi() {
        return new Docket(DocumentationType.SWAGGER_12)
                .groupName("2 - Admin Documentation")
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(AdminRest.class))
                .paths(PathSelectors.any())
                .build().apiInfo(
                        new ApiInfoBuilder()
                                .title("Test Online Dans Multi Pro Admin Documentation")
                                .description("API Documentation with json structure / formatting," +
                                        "Rest Method, e.t.c for KSI Application")
                                .license("Apache 2.0")
                                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                                .termsOfServiceUrl("")
                                .version("1.0.0")
                                .contact(new Contact("", "", "support@test.com"))
                                .build()
                );
    }

    @Bean
    public Docket universalApi() {
        return new Docket(DocumentationType.SWAGGER_12)
                .groupName("3 - Universal Documentation")
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(UniversalRest.class))
                .paths(PathSelectors.any())
                .build().apiInfo(
                        new ApiInfoBuilder()
                                .title("Test Online Dans Multi Pro Admin Documentation")
                                .description("API Documentation with json structure / formatting," +
                                        "Rest Method, e.t.c for KSI Application")
                                .license("Apache 2.0")
                                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                                .termsOfServiceUrl("")
                                .version("1.0.0")
                                .contact(new Contact("", "", "support@test.com"))
                                .build()
                );
    }

    @Bean
    Gson gson() {
        GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat(DateAppConfig.API_DATE_FORMAT);
        return gsonBuilder.create();
    }

//    @Bean
//    public MethodValidationPostProcessor methodValidationPostProcessor() {
//        return new MethodValidationPostProcessor();
//    }

//    @Bean
//    public DataSource dataSource() throws SQLException {
//        return new HikariDataSource(this);
//    }

    @Autowired
    MyRequestInterceptor requestInterceptor ;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }

            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(requestInterceptor).addPathPatterns("/**/**/**/");
            }
        };
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("classpath:messages/messages", "classpath:messages/notification", "classpath:messages/adminActivity", "classpath:messages/validation");
        // If true, the key of the message will be displayed if the key is not found, instead of throwing an exception
        messageSource.setUseCodeAsDefaultMessage(true);
        messageSource.setDefaultEncoding("UTF-8");
        // The value 0 means always reload the messages to be developer friendly
        messageSource.setCacheSeconds(0);
        return messageSource;
    }


    @Bean
    public LocaleResolver localeResolver() {
        return new SmartLocaleResolver();
    }


    public class SmartLocaleResolver extends CookieLocaleResolver {

        @Override
        public Locale resolveLocale(HttpServletRequest request) {
            for (String httpHeaderName : Collections.list(request.getHeaderNames())) {
                logger4j.debug("===========>> Header name: " + httpHeaderName);
            }
            String acceptLanguage = request.getHeader(HttpHeaders.ACCEPT_LANGUAGE);
            logger4j.debug("===========>> acceptLanguage: " + acceptLanguage);
            Locale locale = super.resolveLocale(request);
            logger4j.debug("===========>> acceptLanguage locale: " + locale.getDisplayCountry());
            if (null == locale) {
                locale = getDefaultLocale();
                logger4j.debug("===========>> Default locale: " + locale.getDisplayCountry());
            }
            return locale;
        }

    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public LocalValidatorFactoryBean validator(MessageSource messageSource) {
        LocalValidatorFactoryBean validatorFactoryBean = new LocalValidatorFactoryBean();
        validatorFactoryBean.setValidationMessageSource(messageSource);
        return validatorFactoryBean;
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
