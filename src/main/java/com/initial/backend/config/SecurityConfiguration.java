package com.initial.backend.config;

import com.initial.backend.util.Constant;
import com.initial.backend.security.JwtConfigurer;
import com.initial.backend.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.client.RestTemplate;


@Configuration
@EnableWebSecurity
@PropertySource("classpath:application.properties")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Value("${swagger-username}")
    private String swaggerUsername;

    @Value("${swagger-password}")
    private String swaggerPassword;
    @Value("${secure.swagger}")
    private boolean secureSwagger;


    @Autowired
    RestTemplate restTemplate ;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        if(secureSwagger){
            http.csrf().disable().authorizeRequests()
                    .antMatchers("/api**").permitAll()
                    .antMatchers("/swagger-ui.html**").authenticated().and().formLogin()
                    .and()
                    .apply(new JwtConfigurer(jwtTokenProvider));
        }else{
            http.csrf().disable().authorizeRequests()
                    .antMatchers("/api**").permitAll()
                    .and()
                    .apply(new JwtConfigurer(jwtTokenProvider));
        }
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication().withUser(swaggerUsername).password(swaggerPassword).authorities(Constant.SWAGGER);
    }
}
