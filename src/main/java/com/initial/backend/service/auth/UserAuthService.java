package com.initial.backend.service.auth;


import com.initial.backend.entity.User;
import com.initial.backend.service.test.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserAuthService implements UserDetailsService {

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return null ;
    }

    public UserDetails loadUserByEmail(String email){
        User user = userService.signin(email);
        return user;
    }

}
