package com.initial.backend.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.initial.backend.entity.BasicField;
import com.initial.backend.util.Constant;
import com.initial.backend.exception.AppException;
import com.initial.backend.security.JwtTokenProvider;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.persistence.MappedSuperclass;
import java.util.*;

@MappedSuperclass
public abstract class BasicRepoService<T extends BasicField>  {
    private static final Logger logger = LoggerFactory.getLogger(BasicRepoService.class.getName());

    @Autowired protected MessageSource messageSource;
    @Autowired protected ModelMapper mapper;
    @Autowired protected JwtTokenProvider jwtTokenProvider;
    @Autowired protected RestTemplate restTemplate;
//    @Autowired protected FileStorage fileStorage;
    @Autowired protected EntityManager entityManager ;
//    @Autowired protected AppCacheManager appCacheManager ;



    @Value("${page.row}")
    protected int pageRow ;

    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();

    public List<T> findAll() {
        return repository().findAll();
    }

    public List<T> findAll(Sort sort) {
        return repository().findAll(sort);
    }

    public Page<T> findAll(Pageable pageable) {
        return repository().findAll(pageable);
    }

    public T findById(Long id) {
        Optional<T> optionalT = repository().findById(id);
        if(!optionalT.isPresent()){
            throw new AppException(Constant.FAILED_CODE, message("not.found.on.the.system", "id"));
        }
        return optionalT.get();
    }

    public void delete(Long id) {
        repository().deleteById(id);
    }

    @Transactional
    public T save(T object){
//        Object o = repository().findById(object.getId());
//        Utils.merge(o, object, object.getClass());
//        if(object.isNew()){
//            object = entityManager.merge(object);
//            return object ;
//        }else{
//            entityManager.persist(object);
//        }
        object.setUpdated(new Date());
        repository().save(object);
//        if(object.getId()!=null){
//            try{
//                object = (T) ((Session)entityManager.getDelegate()).merge(object);
//            }catch (Exception e){
//
//            }
//            ((Session)entityManager.getDelegate()).saveOrUpdate(object);
//        }else{
//            Long id = (Long) ((Session)entityManager.getDelegate()).save(object);
//            object.setId(id);
//        }
        return object ;
    }

    public <O extends BasicField> void checkForDelete(Set<O> newList, Set<O> previousList){
        if(newList==null || newList.size()==0){
            if(previousList!=null){
                previousList.forEach(o -> {
                    try{
                        delete(o.getId());
                    }catch (Exception e){
                    }
                });

            }
            return;
        }
        if(previousList!=null){
            previousList.forEach(o -> {
                boolean found = false ;
                for (O o1 : newList) {
                    if(o1.getId()!=null && o.getId()!=null && o1.getId().equals(o.getId()) || o1.getId()==o.getId()){
                        found = true ;
                    }
                }
                if(!found){
                    try{
                        delete(o.getId());
                    }catch (Exception e){
                    }
                }
            });
        }
    }

    protected String message(String message, Object... args) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(message, args==null?new Object[]{}:args, locale);
    }

    public abstract <JPA extends JpaRepository> JPA repository();


    public T createIfNotExist(Class clazz, T object){
        if(object==null || object.getId()==null){
            try {
                object = (T) clazz.newInstance();
                object = (T) repository().save(object);

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
        return object ;
    }

}
