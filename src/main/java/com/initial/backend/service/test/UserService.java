package com.initial.backend.service.test;

import com.initial.backend.entity.User;
import com.initial.backend.exception.AppException;
import com.initial.backend.repository.misc.UserRepository;
import com.initial.backend.service.BasicRepoService;
import com.initial.backend.util.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class UserService extends BasicRepoService<User> {
    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired private UserRepository repository;

    @Override
    public JpaRepository repository(){return repository;}

    public User signin(String email){
        Optional<User> user = repository.findByEmail(email);
        if(!user.isPresent()){
            throw new AppException(Constant.FAILED_CODE, "user not found");
        }
        return user.get();
    }


    public Boolean checkFindByEmailIsRegister(String email){
        Boolean result = false;

        try{
            User user = repository.findAllByEmailAndActiveIsTrue(email);
            if(user!=null){
                result = true;
            }else{
                result = false;
            }
        }catch (Exception e){
            result = true;
            e.printStackTrace();
            logger.error("error", e.getMessage());
        }
        return result;
    }

    public List<User> findAll(Boolean active, String sortir, Boolean ascending){
        Sort sort = new Sort(ascending?Sort.Direction.ASC:Sort.Direction.DESC, sortir);
        List<User> users = new ArrayList<>();
        if(active!=null){
            users = repository.findByActive(active, sort);
        }else{
            users = repository.findAll(sort);
        }

        return users;

    }


}
