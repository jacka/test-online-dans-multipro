package com.initial.backend.service.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.initial.backend.model.PositionRecrutmentDTO;
import com.initial.backend.model.WSResponse;
import com.initial.backend.model.misc.DataPage;
import com.initial.backend.model.misc.GeneralResponse;
import com.initial.backend.util.Constant;
import org.apache.commons.lang.BooleanUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.DateFormatConverter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayOutputStream;
import java.util.*;

@Service
public class PositionRecrutmentService {
    public static final Logger logger = LoggerFactory.getLogger(PositionRecrutmentService.class);
    @Autowired
    RestTemplate restTemplate;

    @Autowired Gson gson ;

    public DataPage findAll(Boolean list, Integer page, String sortir, Boolean ascending){
        String url = "http://dev3.dansmultipro.co.id/api/recruitment/positions.json";

        DataPage dataPage = null;
        List<PositionRecrutmentDTO> recrutmentDTOS = new ArrayList<>();
        try{
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
            JSONArray jsonArray = null;
            try {
                 jsonArray = new JSONArray(responseEntity.getBody().toString());
            }catch (JSONException err){
                logger.error("Error", err.toString());
            }

            if(jsonArray!=null){
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject explrObject = jsonArray.getJSONObject(i);
                    PositionRecrutmentDTO positionRecrutmentDTO = gson.fromJson(explrObject.toString(), PositionRecrutmentDTO.class);
                    recrutmentDTOS.add(positionRecrutmentDTO);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("error", e.getMessage());
            recrutmentDTOS = new ArrayList<>();
        }

        Sort sort = new Sort(ascending?Sort.Direction.ASC:Sort.Direction.DESC, sortir);
        Pageable pageable = PageRequest.of(page, Constant.ROW_LIMIT, sort);
        int max = (Constant.ROW_LIMIT * (page + 1) > recrutmentDTOS.size() ? recrutmentDTOS.size() : Constant.ROW_LIMIT);

        if(BooleanUtils.isFalse(list)){
            try{
                Page<PositionRecrutmentDTO> dtoPage = new PageImpl<>(recrutmentDTOS.subList(page * Constant.ROW_LIMIT, max), pageable, recrutmentDTOS.size());
                dataPage = DataPage.builder(dtoPage.getContent(), Constant.ROW_LIMIT, dtoPage.getTotalElements());
            }catch (Exception e){
                e.printStackTrace();
                dataPage = null;
            }
        }else{
            dataPage = DataPage.builder(recrutmentDTOS, 1000, recrutmentDTOS.size());
        }

        return dataPage;
    }

    public PositionRecrutmentDTO getOne(String id){
        String url = "http://dev3.dansmultipro.co.id/api/recruitment/positions/"+id;

        PositionRecrutmentDTO positionRecrutmentDTO = null;
        try{
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(responseEntity.getBody().toString());
                positionRecrutmentDTO = gson.fromJson(jsonObject.toString(), PositionRecrutmentDTO.class);

                if(positionRecrutmentDTO.getId()==null){
                    positionRecrutmentDTO = null;
                }
            }catch (JSONException err){
                logger.error("Error", err.toString());
                positionRecrutmentDTO = null;
            }

        }catch (Exception e){
            e.printStackTrace();
            logger.error("error", e.getMessage());
            positionRecrutmentDTO = null;
        }
        return positionRecrutmentDTO;
    }

    public byte[] excel(List<PositionRecrutmentDTO> list) throws Exception{
        String[] COLUMNS = {
                "No",
                "Id",
                "type",
                "url",
                "company",
                "location",
                "title",
                "company url",
                "how to apply",
                "link company logo",
        };

        HSSFWorkbook workbook = new HSSFWorkbook();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        CreationHelper creationHelper = workbook.getCreationHelper();

        HSSFSheet sheet = workbook.createSheet("LIST POSITION RECRUTMENT");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.BLACK.getIndex());
        headerFont.setFontHeightInPoints((short)11);

        CellStyle headeCellStyle = workbook.createCellStyle();
        headeCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for(int col=0; col<COLUMNS.length;col++){
            Cell cell = headerRow.createCell(col);
            cell.setCellValue(COLUMNS[col]);
            cell.setCellStyle(headeCellStyle);
            sheet.autoSizeColumn(col, true);
        }

        CellStyle dataCellStyle = workbook.createCellStyle();

        CellStyle ageCellStyle = workbook.createCellStyle();
        ageCellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("#"));

        int rowIdx = 1;

        for(PositionRecrutmentDTO recrutmentDTO : list){
            try{

                HSSFRow row = sheet.createRow(rowIdx++);

                if(recrutmentDTO!=null){
                    row.createCell(0).setCellValue(rowIdx-1);
                    row.createCell(1).setCellValue(recrutmentDTO.getId());
                    row.createCell(2).setCellValue(recrutmentDTO.getType());
                    row.createCell(3).setCellValue(recrutmentDTO.getUrl());
                    row.createCell(4).setCellValue(recrutmentDTO.getCompany());
                    row.createCell(5).setCellValue(recrutmentDTO.getLocation());
                    row.createCell(6).setCellValue(recrutmentDTO.getTitle());
                    row.createCell(7).setCellValue(recrutmentDTO.getCompany_url());
                    row.createCell(8).setCellValue(recrutmentDTO.getHow_to_apply());
                    row.createCell(9).setCellValue(recrutmentDTO.getCompany_logo());
                    row.createCell(10).setCellValue(recrutmentDTO.getCreated_at());



//                    String format = DateFormatConverter.convert(Locale.ENGLISH, "M/d/yy h:mm");
//                    dataCellStyle.setDataFormat(creationHelper.createDataFormat().getFormat(format));
//
//                    HSSFCell cell = row.getCell(11);
//                    cell.setCellValue(new Date());
//                    cell.setCellStyle(dataCellStyle);

                }

            }catch (Exception e){
                e.printStackTrace();
                continue;
            }
        }

        workbook.write(out);
        return out.toByteArray();
    }
}
