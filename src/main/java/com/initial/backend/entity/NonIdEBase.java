package com.initial.backend.entity;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;


@MappedSuperclass
public class NonIdEBase extends BasicField implements Serializable {
    @Id
    protected Long id ;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id ;
    }
}
