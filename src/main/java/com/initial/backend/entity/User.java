package com.initial.backend.entity;

import hindia.Sumatera;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Sumatera
@Data
@Table(name = "j_user")
@Entity
@ToString(of="id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class User extends EBase implements UserDetails {

    @Column(nullable = false)
    String email;

    @Column(nullable = false)
    String password;

    @Column(nullable = false)
    String role;

    @Transient
    String authorization;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList("user").stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());

    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonExpired(){return true;}

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isEnabled(){return true;}

    @Override
    public void initTransient(){
        super.initTransient();
    }

}
